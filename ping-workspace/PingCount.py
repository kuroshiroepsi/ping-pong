#!/usr/bin/python
from flask import Flask, jsonify
from raven import Client

##Sentry client
client = Client('https://57d9800b07704596ab3a64b8dc6e7990:16f80ee599994e1cb7a990be13dc50d0@sentry.io/258354')

count = 0

app = Flask(__name__)

@app.route('/ping', methods=['GET'])
def get_ping():
    global count
    count = count + 1
    try:
        return jsonify([
            {'message': 'pong'}
            ])
    except:
        client.captureException()

@app.route('/count', methods=['GET'])
def get_pong():
    try:
        return jsonify([
            {'pingCount': count}
            ])
    except:
        client.captureException()

##Reset count to 0
@app.route('/reset', methods=['GET'])
def reset():
    global count
    try:
        count = 0
        return ('', 204)
    except:
        client.captureException()

if __name__ == '__main__':
    app.run(host='0.0.0.0')
