[![pipeline status](https://gitlab.com/kuroshirokun/ping-pong/badges/master/pipeline.svg)](https://gitlab.com/kuroshirokun/ping-pong/commits/master)

# Application PingCount

backend web qui accepte 2 routes REST /ping et /count.

Technologies utilisées
----------------------

Docker, Python, Flask, Sentry

![Illustration](resources/docker.png)
![Illustration](resources/python.png)

![Illustration](resources/flask.png)
![Illustration](resources/sentry.png)


Fonctionnement
--------------
- /ping : renvoie le message  { message : "pong" } en JSON et incrémente le compteur de pings
- /count : renvoie la valeur du compteur de pings en JSON : { pingCount : 1 }

Installation
------------
- Installer docker compose à l'aide de la [documentation](https://docs.docker.com/compose/install/).
- Télécharger le fichier docker-compose.yml
- Lancer la commande : ```docker-compose up```